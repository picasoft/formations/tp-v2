#!/bin/bash

option="$@"

case $option in
	1)
	filecheck=".kaamelott.merge_simple.md5"
	;;
	2)
	filecheck=".kaamelott.rebase_simple.md5"
	;;
	3)
	filecheck=".kaamelott.merge_conflits.md5"
	;;
	4)
	filecheck=".kaamelott.rebase_conflits.md5"
	;;
	*)
	echo "Option non prévue !"
	echo "Utilisation : ./check.sh <numéro>"
	echo "1 : vérification après le merge simple"
	echo "2 : vérification après le rebase simple"
	echo "3 : vérification après le merge avec conflits"
	echo "4 : vérification après le rebase avec conflits"
	exit 1
	;;
esac

md5sum --check $filecheck

if [ $? -eq 0 ]
then
	echo "C'est bon, on passe à la suite !"
else
	echo "C'est pas encore ça, vérifiez bien les espaces partout si vous êtes sûrs de vous !"
fi
